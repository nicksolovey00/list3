public class Consigment {
    private String description;
    private Coverable[] coverables;

    public Consigment(String description, Coverable... coverables) {
        this.description = description;
        this.coverables = coverables;
    }

    public double getConsigmentMass(){
        double mass = 0;
        for(Coverable c: coverables){
            mass += c.getGrossMass();
        }
        return mass;
    }

    public String getDescription() {
        return description;
    }

    public Coverable[] getCoverables() {
        return coverables;
    }
}
