public interface Coverable {
    String getName();

    double getNetMass();

    double getGrossMass();
}
