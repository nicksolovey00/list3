import java.util.Objects;

public class CoveredPieceProduct extends PieceProduct implements Coverable{
    private int amountOfPieces;
    private Cover productCover;

    public CoveredPieceProduct(String name, String description, double pieceWeight, int amountOfPieces, Cover productCover) {
        super(name, description, pieceWeight);
        this.amountOfPieces = amountOfPieces;
        this.productCover = productCover;
    }

    public int getAmountOfPieces() {
        return amountOfPieces;
    }

    public Cover getProductCover() {
        return productCover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CoveredPieceProduct that = (CoveredPieceProduct) o;
        return amountOfPieces == that.amountOfPieces &&
                Objects.equals(productCover, that.productCover);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), amountOfPieces, productCover);
    }

    @Override
    public double getNetMass() {
        return getPieceWeight()*amountOfPieces;
    }

    @Override
    public double getGrossMass() {
        return getNetMass() + productCover.getCoverWeight();
    }
}
