import javax.sql.ConnectionEventListener;
import java.util.Objects;

public class CoveredWeightProduct extends WeightProduct implements Coverable {
    private double productWeight;
    private Cover productCover;

    public CoveredWeightProduct(String name, String description, double productWeight, Cover productCover) {
        super(name, description);
        this.productWeight = productWeight;
        this.productCover = productCover;
    }

    public double getProductWeight() {
        return productWeight;
    }

    public Cover getProductCover() {
        return productCover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        CoveredWeightProduct that = (CoveredWeightProduct) o;
        return Double.compare(that.productWeight, productWeight) == 0 &&
                Objects.equals(productCover, that.productCover);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), productWeight, productCover);
    }

    @Override
    public double getNetMass() {
        return productWeight;
    }

    @Override
    public double getGrossMass() {
        return getNetMass() + productCover.getCoverWeight();
    }
}

