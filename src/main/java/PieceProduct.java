import java.util.Objects;

public class PieceProduct extends Product {
    private double pieceWeight;

    public PieceProduct(String name, String description, double pieceWeight) {
        super(name, description);
        this.pieceWeight = pieceWeight;
    }

    public double getPieceWeight() {
        return pieceWeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PieceProduct that = (PieceProduct) o;
        return Double.compare(that.pieceWeight, pieceWeight) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pieceWeight);
    }
}
