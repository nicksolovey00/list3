import filter.Filter;

public class ProductServise {
    public static int countByFilter(Consigment consigment, Filter filter){
        int count = 0;
        for(Coverable c: consigment.getCoverables()){
            if(filter.apply(c.getName())){
                count++;
            }
        }
        return count;
    }
}
