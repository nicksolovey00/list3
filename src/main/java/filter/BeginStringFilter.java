package filter;

public class BeginStringFilter implements Filter{
    String pattern;

    public BeginStringFilter(String pattern) {
        this.pattern = pattern;
    }


    @Override
    public boolean apply(String string) {
        return string.startsWith(pattern);
    }
}
