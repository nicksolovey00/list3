package filter;

public class EndStringFilter implements Filter{
    String pattern;

    public EndStringFilter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean apply(String string) {
        return string.endsWith(pattern);
    }
}
