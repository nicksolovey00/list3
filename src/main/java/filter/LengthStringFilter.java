package filter;

public class LengthStringFilter implements Filter {
    int length;

    public LengthStringFilter(int length) {
        this.length = length;
    }

    @Override
    public boolean apply(String string) {
        return string.length() == length;
    }
}
