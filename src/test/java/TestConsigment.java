import filter.BeginStringFilter;
import org.junit.Assert;
import org.junit.Test;

public class TestConsigment {
    CoveredPieceProduct coveredPieceProduct = new CoveredPieceProduct("Candies", "Sweets",
            0.01, 10, new Cover("Candies cover", 0.02));
    CoveredWeightProduct coveredWeightProduct = new CoveredWeightProduct("Phone", "Communication",
            0.2, new Cover("Phone cover", 0.04));
    Consigment consigment = new Consigment("Phone and Candies", coveredPieceProduct, coveredWeightProduct);

    @Test
    public void testGetConsigmentMass(){
        Assert.assertEquals(0.36, consigment.getConsigmentMass(), 1e-10);
    }

    @Test
    public void testCountByFilter(){
        BeginStringFilter beginStringFilter = new BeginStringFilter("Can");
        Assert.assertEquals(1, ProductServise.countByFilter(consigment, beginStringFilter));
    }
}
