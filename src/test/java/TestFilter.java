import filter.BeginStringFilter;
import filter.EndStringFilter;
import filter.LengthStringFilter;
import org.junit.Assert;
import org.junit.Test;

public class TestFilter {
    @Test
    public void testBeginStringFilter(){
        BeginStringFilter beginStringFilter = new BeginStringFilter("Hell");
        Assert.assertTrue(beginStringFilter.apply("Hello"));
    }

    @Test
    public void testEndStringFilter(){
        EndStringFilter endStringFilter = new EndStringFilter("lo");
        Assert.assertTrue(endStringFilter.apply("Hello"));
    }

    @Test
    public void testLengthStringFilter(){
        LengthStringFilter lengthStringFilter = new LengthStringFilter(5);
        Assert.assertTrue(lengthStringFilter.apply("Hello"));
    }
}
